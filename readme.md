# Following are the steps for downloading and installation of our package in any Laravel 5 application:

## composer.json

```
"require": {
        "flytedesk/accounting": "dev-master"
    },
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:flytedesk/accounting.git"
    }
    ],
```

## /config/app.php

After you `composer update` add `Flytedesk\Accounting\AccountingServiceProvider::class,` to the service providers in app.php and then run `composer dump-autoload`

## .env file

Add the line `SECURITY_PREFIX = someUnguessableString`

## Routes

The routes have a prefix using the SECURITY_PREFIX set in .env, so you might use something like

`https://flytedesk.com/someUnguessableString/authenticate`

which in the oauth process, returns the token to 

`https://flytedesk.com/someUnguessableString/oauth`

so that there are no conflicts with existing URLs