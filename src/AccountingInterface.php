<?php

namespace Flytedesk\Accounting;

interface AccountingInterface
{

    public function authenticate();
    public function welcomePage();
    public function successUrl();
    public function oauthUrl();
    public function menuUrl();
    public function logout($redirectUrl);
    public function reconnect();
    public function diagnostic();

}