<?php

namespace Flytedesk\Accounting;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

/**
 * class Quickbooks
 * mostly deprecated 
 */
class Quickbooks implements AccountingInterface
{
    protected $preAuthData;

    public function __construct()
    {
        $this->preAuthData = $this->GetPreAuth();
    }

    public function welcomePage() {
        return view('accounting::welcome');
    }

    public function authenticate()
    {
        $realm = $this->preAuthData[1];
        $quickbooks_is_connected = $this->preAuthData[2];
        $quickbooks_CompanyInfo = $this->preAuthData[3];

        return view('accounting::welcome-quickbook')->with('realm', $realm)->with('quickbooks_is_connected', $quickbooks_is_connected)->with('quickbooks_CompanyInfo', $quickbooks_CompanyInfo);

    }

    public function GetPreAuth()
    {
        if(Redis::exists('qb_payment_preauth'))
        {
            return unserialize(Redis::get('qb_payment_preauth'));
        }
        else
        {
            $preAuthObject = new \PreAuth();
            $preAuthData = $preAuthObject->runPreAuth();
            Redis::set('qb_payment_preauth',serialize($preAuthData));
            Redis::expire('qb_payment_preauth', 300);
            return $preAuthData;
        }   
    }


    public function successUrl(){
        return view('accounting::success');
    }

    public function oauthUrl(){
        $obj = new \Diagnostics();
        $obj->oauthUrlHash();
    }

    public function menuUrl(){
        $obj = new \Diagnostics();
        $obj->menuUrlHash();
    }

    public function logout($redirectUrl)
    {
        $obj = new \Logout_Quickbook();
        $obj->logout();
        return redirect($redirectUrl);
    }

    public function reconnect(){
        $obj = new \Reconnect();
        $obj->run();
    }

    public function diagnostic() {
        $obj = new \Diagnostics();
        $obj->run();
    }

}