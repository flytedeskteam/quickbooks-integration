<?php

$thisUrl = getenv('APP_URL');

$urlForShowCustomer = str_replace("authenticate","customerRead",$thisUrl);
$urlForCountCustomer = str_replace("authenticate","customerCount",$thisUrl);
$urlForDeleteCustomer = str_replace("authenticate","customerDelete",$thisUrl);
$urlForAddCustomer = str_replace("authenticate","customerCreate",$thisUrl);
$urlForCustomerUpdate = str_replace("authenticate","customerUpdate",$thisUrl);

$urlForShowInvoice = str_replace("authenticate","invoiceRead",$thisUrl);
$urlForDeleteInvoice = str_replace("authenticate","invoiceDelete",$thisUrl);
$urlForAddInvoice = str_replace("authenticate","invoiceCreate",$thisUrl);
$urlForUpdateInvoice = str_replace("authenticate","invoiceUpdate",$thisUrl);
$urlForAddLineInvoice = str_replace("authenticate","invoiceLineAdd",$thisUrl);
$urlForRemoveLineInvoice = str_replace("authenticate","invoiceLineRemove",$thisUrl);
$urlForSendInvoice = str_replace("authenticate","invoiceLineSend",$thisUrl);

$urlForShowVendor = str_replace("authenticate","vendorRead",$thisUrl);
$urlForAddVendor = str_replace("authenticate","vendorCreate",$thisUrl);
$urlForDeleteVendor = str_replace("authenticate","vendorDelete",$thisUrl);
$urlForUpdateVendor = str_replace("authenticate","vendorUpdate",$thisUrl);

$urlForShowPurchaseOrder = str_replace("authenticate","PORead",$thisUrl);
$urlForAddPurchaseOrder = str_replace("authenticate","POCreate",$thisUrl);
$urlForDeletePurchaseOrder = str_replace("authenticate","PODelete",$thisUrl);
$urlForUpdatePurchaseOrder = str_replace("authenticate","POUpdate",$thisUrl);
$urlForAddLineToPurchaseOrder = str_replace("authenticate","POLineAdd",$thisUrl);
$urlForRemoveLineFromPurchaseOrder = str_replace("authenticate","POLineRemove",$thisUrl);

$urlForShowEmployee = str_replace("authenticate","employeeRead",$thisUrl);
$urlForShowPayment = str_replace("authenticate","paymentRead",$thisUrl);
$urlForCustomerPayment = str_replace("authenticate","customerPaymentAccept",$thisUrl);

$urlForLogout = str_replace("authenticate","logout",$thisUrl);
$urlForReconnect = str_replace("authenticate","reconnect",$thisUrl);
$urlForDiagnostic = str_replace("authenticate","diagnose",$thisUrl);


require_once dirname(__FILE__) . '/../../../vendor/flytedesk/accounting/src/views/header.tpl.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title>QuickBooks</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>

        .title {
            font-size: 96px;
            text-align: center;
            vertical-align: middle;
        }

        .caption {
            font-size: small;
        }

        table
        {
            margin-left: 20px;
            margin-right: 20px;
        }

        td
        {
            padding: 4px;
        }

    </style>
</head>
<body><br><br><h1><center>WELCOME TO QUICKBOOKS!</center></h1><br><br>


    <div class="title"><ipp:connectToIntuit></ipp:connectToIntuit></div><br>
    <div class="caption"><center><b>click the above button to be connected to QuickBooks if you are not connected</b></center></div>
    <br><br><br>
    <?php if ($quickbooks_is_connected): ?>

        <div style="border: 2px solid green; text-align: center; padding: 20px; color: green;">
            CONNECTED!<br>
            <br>
            <i>
                Realm: <?php print($realm); ?><br>
                Company: <?php print($quickbooks_CompanyInfo->getCompanyName()); ?><br>
                Email: <?php print($quickbooks_CompanyInfo->getEmail()->getAddress()); ?><br>
                Country: <?php print($quickbooks_CompanyInfo->getCountry()); ?>
            </i>
        </div><br><br>
        <h3>Actions you can perform</h3>
        <table>
            <tr>
                <td>
                    <a href="<?php echo $urlForShowCustomer ?>">Show customer</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForCountCustomer ?>">Count customer</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForAddCustomer ?>">Add customer</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForCustomerUpdate ?>">Update customer</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForDeleteCustomer ?>">Delete customer</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForShowVendor ?>">Show vendor</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForAddVendor ?>">Add vendor</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForUpdateVendor ?>">Update vendor</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForDeleteVendor ?>">Delete vendor</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForShowInvoice ?>">Show Invoices</a>
                </td>
            </tr>

            <tr>
                <td>
                    <a href="<?php echo $urlForAddInvoice ?>">Add invoice</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForDeleteInvoice ?>">Delete invoice</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForUpdateInvoice ?>">Update invoice</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForAddLineInvoice ?>">Add line to invoice</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForRemoveLineInvoice ?>">Remove line from invoice</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForSendInvoice ?>">Send invoice</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForShowPurchaseOrder ?>">Show purchase order</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForAddPurchaseOrder ?>">Add purchase order</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForDeletePurchaseOrder ?>">Delete purchase order</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForUpdatePurchaseOrder ?>">Update purchase order</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForAddLineToPurchaseOrder ?>">Add line to purchase order</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForRemoveLineFromPurchaseOrder ?>">Remove line from purchase order</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForShowEmployee ?>">Show employee</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForShowPayment ?>">Show payment</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForCustomerPayment ?>">Customer payment</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForLogout ?>">logout</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForReconnect ?>">Reconnect / refresh connection</a>
                </td>
                <td>
                    (QuickBooks connections expire after 6 months, so you have to this roughly every 5 and 1/2 months)
                </td>

            </tr>
            <tr>
                <td>
                    <a href="<?php echo $urlForDiagnostic ?>">Diagnostics about QuickBooks connection</a>
                </td>
             </tr>
        </table>


    <?php endif; ?>

</body>
</html>



