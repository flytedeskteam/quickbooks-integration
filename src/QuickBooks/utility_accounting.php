<?php

require_once dirname(__DIR__) . '/QuickBooks/QuickBooks.php';

use Illuminate\Support\Facades\Config;

class Logout_Quickbook
{
    public function logout()
    {
        $the_username = Config::get('quickbooks.the_username');
        $the_tenant = Config::get('quickbooks.the_tenant');

        $baseUrl = getenv('APP_URL');
        $dsn = Config::get('quickbooks.dsn');
        $encryption_key = Config::get('quickbooks.encryption_key');
        $oauth_consumer_key = Config::get('quickbooks.oauth_consumer_key');
        $oauth_consumer_secret = Config::get('quickbooks.oauth_consumer_secret');
        $quickbooks_oauth_url = $baseUrl.'/'.Config::get('quickbooks.security_prefix').'/oauthUrl';
        $quickbooks_success_url = $baseUrl.'/'.Config::get('quickbooks.security_prefix').'/successUrl';

        $IntuitAnywhere = new QuickBooks_IPP_IntuitAnywhere($dsn, $encryption_key, $oauth_consumer_key, $oauth_consumer_secret, $quickbooks_oauth_url, $quickbooks_success_url);

        if ($IntuitAnywhere->disconnect($the_username, $the_tenant)) {
        }
    }
}

Class Reconnect
{
    public function run()
    {
        $the_username = Config::get('quickbooks.the_username');
        $the_tenant = Config::get('quickbooks.the_tenant');

        $baseUrl = getenv('APP_URL');
        $dsn = Config::get('quickbooks.dsn');
        $encryption_key = Config::get('quickbooks.encryption_key');
        $oauth_consumer_key = Config::get('quickbooks.oauth_consumer_key');
        $oauth_consumer_secret = Config::get('quickbooks.oauth_consumer_secret');
        $quickbooks_oauth_url = $baseUrl.'/'.Config::get('quickbooks.security_prefix').'/oauthUrl';
        $quickbooks_success_url = $baseUrl.'/'.Config::get('quickbooks.security_prefix').'/successUrl';

        $IntuitAnywhere = new QuickBooks_IPP_IntuitAnywhere($dsn, $encryption_key, $oauth_consumer_key, $oauth_consumer_secret, $quickbooks_oauth_url, $quickbooks_success_url);

        $reconnected = false;
        $err = '';
        $expiry = $IntuitAnywhere->expiry($the_username, $the_tenant);
        if ($expiry == QuickBooks_IPP_IntuitAnywhere::EXPIRY_SOON) {
            if ($IntuitAnywhere->reconnect($the_username, $the_tenant)) {
                $reconnected = true;
            } else {
                $reconnected = false;
                $err = $IntuitAnywhere->errorNumber() . ': ' . $IntuitAnywhere->errorMessage();
            }

            //print_r($IntuitAnywhere->load($the_username, $the_tenant));
            //print("\n\n\n");
            //print($IntuitAnywhere->lastRequest());
            //print("\n\n\n");
            //print($IntuitAnywhere->lastResponse());

        } else if ($expiry == QuickBooks_IPP_IntuitAnywhere::EXPIRY_NOTYET) {
            $err = 'This connection is not old enough to require reconnect/refresh.';
        } else if ($expiry == QuickBooks_IPP_IntuitAnywhere::EXPIRY_EXPIRED) {
            $err = 'This connection has already expired. You\'ll have to go through the initial connection process again.';
        } else if ($expiry == QuickBooks_IPP_IntuitAnywhere::EXPIRY_UNKNOWN) {
            $err = 'Are you sure you\'re connected? No connection information was found for this user/tenant...';
        }
        if ($reconnected): {
            print('RECONNECTED! (refreshed OAuth tokens)');
        } else: {
            print('ERROR:');
            print($err);
        }
        endif;
    }
}

Class Diagnostics
{
    public function run()
    {
        $the_username = Config::get('quickbooks.the_username');
        $the_tenant = Config::get('quickbooks.the_tenant');

        $baseUrl = getenv('APP_URL');
        $dsn = Config::get('quickbooks.dsn');
        $encryption_key = Config::get('quickbooks.encryption_key');
        $oauth_consumer_key = Config::get('quickbooks.oauth_consumer_key');
        $oauth_consumer_secret = Config::get('quickbooks.oauth_consumer_secret');
        $quickbooks_oauth_url = $baseUrl.'/'.Config::get('quickbooks.security_prefix').'/oauthUrl';
        $quickbooks_success_url = $baseUrl.'/'.Config::get('quickbooks.security_prefix').'/successUrl';

        $IntuitAnywhere = new QuickBooks_IPP_IntuitAnywhere($dsn, $encryption_key, $oauth_consumer_key, $oauth_consumer_secret, $quickbooks_oauth_url, $quickbooks_success_url);

        $check = $IntuitAnywhere->check($the_username, $the_tenant);
        $test = $IntuitAnywhere->test($the_username, $the_tenant);
        $creds = $IntuitAnywhere->load($the_username, $the_tenant);
        $diagnostics = array_merge(array(
            'check' => $check,
            'test' => $test,
        ), (array)$creds);
        print_r($diagnostics);
    }

    public function oauthUrlHash(){

        $the_username = Config::get('quickbooks.the_username');
        $the_tenant = Config::get('quickbooks.the_tenant');

        $baseUrl = getenv('APP_URL');
        $dsn = Config::get('quickbooks.dsn');
        $encryption_key = Config::get('quickbooks.encryption_key');
        $oauth_consumer_key = Config::get('quickbooks.oauth_consumer_key');
        $oauth_consumer_secret = Config::get('quickbooks.oauth_consumer_secret');
        $quickbooks_oauth_url = $baseUrl.'/'.Config::get('quickbooks.security_prefix').'/oauthUrl';
        $quickbooks_success_url = $baseUrl.'/'.Config::get('quickbooks.security_prefix').'/successUrl';

        $IntuitAnywhere = new QuickBooks_IPP_IntuitAnywhere($dsn, $encryption_key, $oauth_consumer_key, $oauth_consumer_secret, $quickbooks_oauth_url, $quickbooks_success_url);

        if ($IntuitAnywhere->handle($the_username, $the_tenant))
        {
            ; // The user has been connected, and will be redirected to $that_url automatically.
        }
        else
        {
            // If this happens, something went wrong with the OAuth handshake
            die('Oh no, something bad happened: ' . $IntuitAnywhere->errorNumber() . ': ' . $IntuitAnywhere->errorMessage());
        }
    }

    public function menuUrlHash(){

        $the_username = Config::get('quickbooks.the_username');
        $the_tenant = Config::get('quickbooks.the_tenant');

        $baseUrl = getenv('APP_URL');
        $dsn = Config::get('quickbooks.dsn');
        $encryption_key = Config::get('quickbooks.encryption_key');
        $oauth_consumer_key = Config::get('quickbooks.oauth_consumer_key');
        $oauth_consumer_secret = Config::get('quickbooks.oauth_consumer_secret');
        $quickbooks_oauth_url = $baseUrl.'/'.Config::get('quickbooks.security_prefix').'/oauthUrl';
        $quickbooks_success_url = $baseUrl.'/'.Config::get('quickbooks.security_prefix').'/successUrl';

        $IntuitAnywhere = new QuickBooks_IPP_IntuitAnywhere($dsn, $encryption_key, $oauth_consumer_key, $oauth_consumer_secret, $quickbooks_oauth_url, $quickbooks_success_url);

        die($IntuitAnywhere->widgetMenu($the_username, $the_tenant));
    }
}

Class PreAuth{

    public function runPreAuth() {

        $the_username = Config::get('quickbooks.the_username');
        $the_tenant = Config::get('quickbooks.the_tenant');
        $sandbox = Config::get('quickbooks.sandbox');
        $dsn = Config::get('quickbooks.dsn');

        $baseUrl = getenv('APP_URL');
        $encryption_key = Config::get('quickbooks.encryption_key');
        $oauth_consumer_key = Config::get('quickbooks.oauth_consumer_key');
        $oauth_consumer_secret = Config::get('quickbooks.oauth_consumer_secret');
        $quickbooks_oauth_url = $baseUrl.'/'.Config::get('quickbooks.security_prefix').'/oauthUrl';
        $quickbooks_success_url = $baseUrl.'/'.Config::get('quickbooks.security_prefix').'/successUrl';

        // Instantiate our Intuit Anywhere auth handler
//
// The parameters passed to the constructor are:
//	$dsn
//	$oauth_consumer_key		Intuit will give this to you when you create a new Intuit Anywhere application at AppCenter.Intuit.com
//	$oauth_consumer_secret	Intuit will give this to you too
//	$this_url				This is the full URL (e.g. http://path/to/this/file.php) of THIS SCRIPT
//	$that_url				After the user authenticates, they will be forwarded to this URL
//

        $IntuitAnywhere = new QuickBooks_IPP_IntuitAnywhere($dsn, $encryption_key, $oauth_consumer_key, $oauth_consumer_secret, $quickbooks_oauth_url, $quickbooks_success_url);


        if ($IntuitAnywhere->check($the_username, $the_tenant) and
            $IntuitAnywhere->test($the_username, $the_tenant))
        {
            // Yes, they are

            $quickbooks_is_connected = true;

            // Set up the IPP instance

            $IPP = new QuickBooks_IPP($dsn);

            // Get our OAuth credentials from the database
            $creds = $IntuitAnywhere->load($the_username, $the_tenant);

            // Tell the framework to load some data from the OAuth store
            $IPP->authMode(
                QuickBooks_IPP::AUTHMODE_OAUTH,
                $the_username,
                $creds);

            if ($sandbox)
            {
                // Turn on sandbox mode/URLs
                $IPP->sandbox(true);
            }

            // Print the credentials we're using
            //print_r($creds);

            // This is our current realm

            $realm = $creds['qb_realm'];
            // Load the OAuth information from the database

            $Context= $IPP->context();

            // Get some company info
            $CompanyInfoService = new QuickBooks_IPP_Service_CompanyInfo();

            $quickbooks_CompanyInfo = $CompanyInfoService->get($Context, $realm);

        }
        else
        {
            // No, they are not
            $quickbooks_is_connected = false;
            $quickbooks_CompanyInfo = null;
        }

        $Context = isset($Context) ? $Context : null;
        $realm = isset($realm) ? $realm : null;

        $returnArray = array();
        $returnArray[0] = $Context;
        $returnArray[1] = $realm;
        $returnArray[2] = $quickbooks_is_connected;
        $returnArray[3] = $quickbooks_CompanyInfo;
        return $returnArray;
    }

}


