<?php
class Customer
{
    public $id;
    public $title;
    public $givenName;
    public $middleName;
    public $familyName;
    public $displayName;
    public $primaryPhoneNumber;
    public $mobileNumber;
    public $faxNumber;
    public $billAddrLine1;
    public $billAddrLine2;
    public $billAddrCity;
    public $billAddrCountrySubDivisionCode;
    public $billAddrPostalCode;
    public $primaryEmailAddress;
    public $companyName;
    public $acctNum;
    public $preferredDeliveryMethod;

    public function __construct($Context, $Realm, $flytedeskBuyer)
    {
        $this->Context = $Context;
        $this->Realm = $Realm;
        $this->flytedeskBuyer = $flytedeskBuyer;
        $this->CustomerService = new QuickBooks_IPP_Service_CustomerService();
    }

    public function setAttributes($attributes)
    {
        foreach ($attributes as $key => $value)
        {
            $this->{$key} = $value;
        }
    }

    public function getCountOfCustomers($Context, $realm)
    {
        $count = $this->CustomerService->query($this->Context, $this->Realm, "SELECT COUNT(*) FROM Customer  ");
        return($count);
    }

    public function showCustomer($Context, $realm)
    {
        $customers = $this->CustomerService->query($this->Context, $this->Realm, "SELECT * FROM Customer MAXRESULTS 25");
        $arrayToSend = array();

        foreach ($customers as $Customer) 
        {
            $arrayToSend[] = array(
                "id" => QuickBooks_IPP_IDS::usableIDType($Customer->getId()),
                "fullyQualifiedName" => $Customer->getFullyQualifiedName()
            );
        }
        return($arrayToSend);
    }

    public function showCustomerByName($name)
    {
        $customers = $this->CustomerService->query($this->Context, $this->Realm, "SELECT * FROM Customer where DisplayName='" . $name . "' MAXRESULTS 25");
        if(!isset($customers[0]))  
        {
            // no existing customer. Return null.
            return null;
        }      
        else
        {
            $arrayToSend = array();
            foreach ($customers as $Customer) 
            {
                $arrayToSend[] = array(
                    "id" => QuickBooks_IPP_IDS::usableIDType($Customer->getId()),
                    "fullyQualifiedName" => $Customer->getFullyQualifiedName()
                );
            }
            return($arrayToSend);
        }
    }

    public function addCustomer()
    {
        $Customer = new QuickBooks_IPP_Object_Customer();
        $Customer->setGivenName($this->flytedeskBuyer->buyer_name);
        $Customer->setDisplayName($this->flytedeskBuyer->buyer_name);
        $Customer->setCompanyName($this->flytedeskBuyer->buyer_company);
        $Customer->setAcctNum(uniqid('customer_'));

        // Terms (e.g. Net 30, etc.)
        // $Customer->setSalesTermRef(7);

        // Phone #
        $PrimaryPhone = new QuickBooks_IPP_Object_PrimaryPhone();
        $PrimaryPhone->setFreeFormNumber($this->flytedeskBuyer->buyer_phone);
        $Customer->setPrimaryPhone($PrimaryPhone);

        // Mobile #
        $Mobile = new QuickBooks_IPP_Object_Mobile();
        $Mobile->setFreeFormNumber($this->flytedeskBuyer->buyer_phone);
        $Customer->setMobile($Mobile);

//        // Fax #
//        $Fax = new QuickBooks_IPP_Object_Fax();
//        $Fax->setFreeFormNumber($this->faxNumber);
//        $Customer->setFax($Fax);

        // Bill address
        $BillAddr = new QuickBooks_IPP_Object_BillAddr();
        $BillAddr->setLine1($this->flytedeskBuyer->buyer_address);
        $BillAddr->setCity($this->flytedeskBuyer->buyer_city);
        $BillAddr->setCountrySubDivisionCode($this->flytedeskBuyer->buyer_state);
        $BillAddr->setPostalCode($this->flytedeskBuyer->buyer_zip_code);
        $Customer->setBillAddr($BillAddr);

        // email
        $PrimaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
        $PrimaryEmailAddr->setAddress($this->flytedeskBuyer->billing_email);
        $Customer->setPrimaryEmailAddr($PrimaryEmailAddr);
        if ($resp = $this->CustomerService->add($this->Context, $this->Realm, $Customer)) 
        {
            return(QuickBooks_IPP_IDS::usableIDType($resp));
        } 
        else 
        {
            return($this->CustomerService->lastError($this->Context));
        }
    }

    public function customerUpdate()
    {
        // Get the existing customer first (you need the latest SyncToken value)
        $customers = $this->CustomerService->query($this->Context, $this->Realm, "SELECT * FROM Customer WHERE Id = '$this->id' ");
        if (!empty($customers)) 
        {
            $Customer = $customers[0];

            // Change something
            if($this->title != null) 
            {
                $Customer->setTitle($this->title);
            }

            if($this->preferredDeliveryMethod != null) 
            {
                $Customer->setPreferredDeliveryMethod($this->preferredDeliveryMethod);
            }

            if($this->companyName != null) 
            {
                $Customer->setCompanyName($this->companyName);
            }

            if($this->givenName != null) 
            {
                $Customer->setGivenName($this->givenName);
            }

            if($this->middleName != null) 
            {
                $Customer->setMiddleName($this->middleName);
            }

            if($this->familyName != null) 
            {
                $Customer->setFamilyName($this->familyName);
            }

            if($this->displayName != null) 
            {
                $Customer->setDisplayName($this->displayName);
            }

            if($this->primaryPhoneNumber != null) 
            {
                $PrimaryPhone = $Customer->getPrimaryPhone();
                if($PrimaryPhone == null)
                {
                    $PrimaryPhone = new QuickBooks_IPP_Object_PrimaryPhone();
                }
                $PrimaryPhone->setFreeFormNumber($this->primaryPhoneNumber);
                $Customer->setPhoneNumber($PrimaryPhone);
            }

            if($this->mobileNumber != null) 
            {
                $Mobile = $Customer->getMobile();
                if($Mobile == null)
                {
                    $Mobile = new QuickBooks_IPP_Object_Mobile();
                }
                $Mobile->setFreeFormNumber($this->mobileNumber);
                $Customer->setMobile($Mobile);
            }

            if($this->faxNumber != null) 
            {
                $Fax = $Customer->getFax();
                if($Fax == null)
                {
                    $Fax = new QuickBooks_IPP_Object_Fax();
                }
                $Fax->setFreeFormNumber($this->faxNumber);
                $Customer->setFax($Fax);
            }

            // update billing address
            $BillAddr = $Customer->getBillAddr();
            if($BillAddr == null)
            {
                $BillAddr = new QuickBooks_IPP_Object_BillAddr();
            }
            if($this->billAddrLine1 != null) 
            {
                $BillAddr->setLine1($this->billAddrLine1);
            }

            if($this->billAddrLine2 != null) 
            {
                $BillAddr->setLine2($this->billAddrLine2);
            }

            if($this->billAddrCity != null) 
            {
                $BillAddr->setCity($this->billAddrCity);
            }

            if($this->billAddrCountrySubDivisionCode != null) 
            {
                $BillAddr->setCountrySubDivisionCode($this->billAddrCountrySubDivisionCode);
            }

            if($this->billAddrPostalCode != null) 
            {
                $BillAddr->setPostalCode($this->billAddrPostalCode);
            }

            // Set email address
            if($this->primaryEmailAddress != null) 
            {
                $PrimaryEmailAddr = $Customer->getPrimaryEmailAddr();
                if($PrimaryEmailAddr == null)
                {
                    $PrimaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
                }
                $PrimaryEmailAddr->setAddress($this->primaryEmailAddress);
                $Customer->setPrimaryEmailAddr($PrimaryEmailAddr);
            }

            // update
            if ($this->CustomerService->update($this->Context, $this->Realm, $Customer->getId(), $Customer)) 
            {
                return true;
            } 
            else 
            {
                return ('&nbsp; Error: ' . $this->CustomerService->lastError($this->Context));
            }
        }
        else
        {
            return ("Could not find Customer " . $this->id . " to update");
        }
    }

    public function customerDelete ()
    {
        $customers = $this->CustomerService->query($this->Context, $this->Realm, "SELECT * FROM Customer WHERE Id = '$this->id' ");
        if(!empty($customers)) 
        {
            $Customer = $customers[0];
            $array = array();
            $array[0] = "false";
            $Customer->setActive($array);   //this actually tell the api to delete the item

            if ($this->CustomerService->update($this->Context, $this->Realm, $Customer->getId(), $Customer)) 
            {
                return true;
            } 
            else 
            {
                return('&nbsp; Error: ' . $this->CustomerService->lastError($Context));
            }
        }
        else 
        {
            return ("Could not find Customer ".$this->id. " to delete");
        }
    }
}



