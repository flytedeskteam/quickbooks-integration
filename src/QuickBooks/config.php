<?php

/**
 * Intuit Partner Platform configuration variables
 * 
 * See the scripts that use these variables for more details. 
 * 
 * @package QuickBooks
 * @subpackage Documentation
 */
use Illuminate\Support\Facades\Config;

// Turn on some error reporting
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Require the library codea
require_once dirname(__DIR__) . '/QuickBooks/QuickBooks.php';

// Your OAuth token (Intuit will give you this when you register an Intuit Anywhere app)
$token = Config::get('quickbooks.token');

// Your OAuth consumer key and secret (Intuit will give you both of these when you register an Intuit app)
// 
// IMPORTANT:
//	To pass your tech review with Intuit, you'll have to AES encrypt these and 
//	store them somewhere safe. 
// 
// The OAuth request/access tokens will be encrypted and stored for you by the 
//	PHP DevKit IntuitAnywhere classes automatically.

$oauth_consumer_key = Config::get('quickbooks.oauth_consumer_key');

$oauth_consumer_secret = Config::get('quickbooks.oauth_consumer_secret');

// If you're using DEVELOPMENT TOKENS, you MUST USE SANDBOX MODE!!!  If you're in PRODUCTION, then DO NOT use sandbox.

$sandbox = Config::get('quickbooks.sandbox');

 // When you're using development tokens
//$sandbox = false;    // When you're using production tokens

$baseUrl = getenv('APP_URL');

$security_prefix = env('SECURITY_PREFIX');

// This is the URL of your OAuth auth handler page
$quickbooks_oauth_url = $baseUrl.'/'.Config::get('quickbooks.security_prefix').'/oauthUrl';

// This is the URL to forward the user to after they have connected to IPP/IDS via OAuth
$quickbooks_success_url = $baseUrl.'/'.Config::get('quickbooks.security_prefix').'/successUrl';

// This is the menu URL script
$quickbooks_menu_url = $baseUrl.'/'.Config::get('quickbooks.security_prefix').'/menuUrl';

// This is a database connection string that will be used to store the OAuth credentials
// $dsn = 'mysql://username:password@hostname/database';
// $dsn = 'mysqli://root:root@localhost/quickbooks';

// This is a laravel specific solution, so... yeah

$dsn = Config::get('quickbooks.dsn');

// You should set this to an encryption key specific to your app
$encryption_key = Config::get('quickbooks.encryption_key');

// Do not change this unless you really know what you're doing!!!  99% of apps will not require a change to this.

$the_username = Config::get('quickbooks.the_username');

// The tenant that user is accessing within your own app

$the_tenant = Config::get('quickbooks.the_tenant');

// Initialize the database tables for storing OAuth information
if (!QuickBooks_Utilities::initialized($dsn))
{
	// Initialize creates the neccessary database schema for queueing up requests and logging
	QuickBooks_Utilities::initialize($dsn);
}

