<?php

class VendorCredit
{
    public $id;
    public $txnDate;
    public $customerRef;
    public $vendorRef;
    public $dueDate;
    public $syncToken;
    public $detailType;
    public $unitPrice;
    public $qty;
    public $description;
    public $lineItems;

    public function __construct($Context, $Realm){
        $this->Context = $Context;
        $this->Realm = $Realm;
        $this->BillService = new QuickBooks_IPP_Service_Bill();
    }
    public function setAttributes($attributes)
    {
        foreach ($attributes as $key => $value) 
        {
            $this->{$key} = $value;
        }
    }

    public function addVendorCredit()
    {
        $VendorCreditService = new QuickBooks_IPP_Service_VendorCredit();
        $VendorCredit = new QuickBooks_IPP_Object_VendorCredit();
        $VendorCredit->setTxnDate($this->txnDate);
        $this->lineItem = new VendorCredit_Line();
        $this->lineItem->setAttributes($this->customerRef, $this->unitPrice);
        $Line = $this->addLineItemsToVendorCredit($this->lineItem);

        $VendorCredit->addLine($Line);
        $VendorCredit->setVendorRef($this->vendorRef);
        $VendorCredit->setCustomerRef($this->customerRef);
        if ($resp = $VendorCreditService->add($this->Context, $this->Realm, $VendorCredit)) 
        {
            return(["Success" => QuickBooks_IPP_IDS::usableIDType($resp)]);
        } 
        else 
        {
            return(["Error" => $VendorCreditService->lastError()]);
        }
    }
    public function addLineItemsToVendorCredit($item)
    {
        $Line = new QuickBooks_IPP_Object_Line();
        $Line->setDetailType($item->detailType);
        $Line->setAmount($item->amount); 
        $AccountBasedExpenseLineDetail = new QuickBooks_IPP_Object_AccountBasedExpenseLineDetail();
        $AccountBasedExpenseLineDetail->setCustomerRef($item->customerRef);
        $AccountBasedExpenseLineDetail->setAccountRef($item->accountRef);
        $Line->addAccountBasedExpenseLineDetail($AccountBasedExpenseLineDetail);

        return $Line;
    }
}

Class VendorCredit_Line 
{
    public function setId($id)
    {
        $this->id = $id;
    }

    public function setAttributes($customerRef, $amount)
    {
        $this->customerRef = $customerRef;
        $this->accountRef = '35'; // TODO: do not hard-code this value
        $this->detailType = 'AccountBasedExpenseLineDetail'; 
        $this->amount = $amount;
    }

}