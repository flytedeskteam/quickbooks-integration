<?php

class Invoice
{
    public $id;
    public $docNumber;
    public $txnDate;
    public $detailType;
    public $description;
    public $itemRef;
    public $unitPrice;
    public $qty;
    public $customerRef;
    public $address;
    public $poNumber;
    public $InvoiceService;

    public function __construct($Context, $Realm)
    {
        $this->Context = $Context;
        $this->Realm = $Realm;
        $this->InvoiceService = new QuickBooks_IPP_Service_Invoice();
    }

    public function setAttributes($attributes)
    {
        foreach ($attributes as $key => $value) 
        {
            $this->{$key} = $value;
        }
    }
    public function getInvoices($Context, $realm)
    {
        $invoices = $this->InvoiceService->query($this->Context, $this->Realm, "SELECT * FROM Invoice STARTPOSITION 1 MAXRESULTS 10");
        $arrayToSend = array();

        foreach ($invoices as $Invoice) 
        {
            $arrayToSend[] = array(
                "docNumber" => $Invoice->getDocNumber(),
                "totalAmt" => $Invoice->getTotalAmt(),
                "invoiceDate" => $invoice->getTxnDate(),
                "description" => $Invoice->getLine(0)->getDescription(),
                "id" => QuickBooks_IPP_IDS::usableIDType($Invoice->getId())
            );
        }
        return($arrayToSend);
    }
    public function getInvoice($invoiceId)
    {
        $invoices = $this->InvoiceService->query($this->Context, $this->Realm, "SELECT * FROM Invoice where id='" . $invoiceId . "' " );
        $arrayToSend = array();
        foreach ($invoices as $invoice)
        {
            $arrayToSend[] = array(
                "docNumber" => $invoice->getDocNumber(),
                "totalAmt" => $invoice->getTotalAmt(),
                "invoiceDate" => $invoice->getTxnDate(),
                "description" => $invoice->getLine(0)->getDescription(),
                "id" => QuickBooks_IPP_IDS::usableIDType($invoice->getId())
            );
        }
        return($arrayToSend);
    }

    public function getInvoicesByClient($clientId)
    {
        $invoices = $this->InvoiceService->query($this->Context, $this->realm, "SELECT * FROM Invoice where customerRef='" . $clientId . "' " );
        $arrayToSend = array();
        foreach ($invoices as $invoice)
        {
            $arrayToSend[] = array(
                "docNumber" => $invoice->getDocNumber(),
                "totalAmt" => $invoice->getTotalAmt(),
                "invoiceDate" => $invoice->getTxnDate(),
                "dueDate"     => $invoice->getDueDate(),
                "balance"     => $invoice->getBalance(),
                "description" => $invoice->getLine(0)->getDescription(),
                "id" => QuickBooks_IPP_IDS::usableIDType($invoice->getId())
            );
        }
        return($arrayToSend);
    }


    public function createNewInvoice()
    {
        $Invoice = new QuickBooks_IPP_Object_Invoice();
        //$Invoice->setDocNumber($this->docNumber);
        $Invoice->setTxnDate($this->txnDate);

        //$SendAddress = new QuickBooks_IPP_Object_BillEmail();
        //$SendAddress->setAddress($this->email);

        $CustomField = new QuickBooks_IPP_Object_CustomField();
        $CustomField->setDefinitionId("1");
        $CustomField->setType('StringType');
        $CustomField->setName("P.O. Number");
        $CustomField->setStringValue($this->docNumber);
        $Invoice->addCustomField($CustomField);

        $Invoice->setSalesTermRef('7');

        foreach ($this->lineItems as $key => $value) 
        {
            $Line = $this->addLineItemsToNewInvoice($value);
            $Invoice->addLine($Line);
        }

        //$Invoice->setBillEmail($SendAddress);
        $Invoice->setCustomerRef($this->customerRef);

        if ($resp = $this->InvoiceService->add($this->Context, $this->Realm, $Invoice)) 
        {
            return(["Id" => QuickBooks_IPP_IDS::usableIDType($resp)]);
        } 
        else 
        {
            return(["Error" => $this->InvoiceService->lastError()]);
        }

    }

    public function addPONumberToInvoice($number)
    {
        $CustomField = new QuickBooks_IPP_Object_CustomField();
        $CustomField->setDefinitionId(1);
        $CustomField->setType('StringType');
        $CustomField->setStringValue($number);
        return $CustomField;
    }

    public function addLineItemsToNewInvoice($item)
    {
	if($item->mediakit_id === 3){
		$unit_price = ($item->cost_fee / $item->quantity);
	} else {
		$unit_price = $item->cost_fee;
	}    
	$description = $item->school_name . ' - ' . $item->publication_name . ': ' . $item->display_name . ' ' . $item->asset_date;
            $Line = new QuickBooks_IPP_Object_Line();
            $Line->setDetailType('SalesItemLineDetail'); // TODO what are the detail types possible
            $Line->setAmount($unit_price * $item->quantity);
            $Line->setDescription($description);
            $SalesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
            //$SalesItemLineDetail->setCustomerRef($item->customerRef);
            $SalesItemLineDetail->setItemRef($item->itemRef);
            $SalesItemLineDetail->setUnitPrice($unit_price);
            $SalesItemLineDetail->setQty($item->quantity);
            $Line->addSalesItemLineDetail($SalesItemLineDetail);

        return $Line;
    }

    public function deleteInvoice($id)
    {
        $the_invoice_to_delete = '{'.'-'.$id.'}';
        $retr = $this->InvoiceService->delete($this->Context, $this->Realm, $the_invoice_to_delete);
        $error = $this->InvoiceService->lastError();
        if(empty($error))
        {
            return true;
        }
        else
        {
            return('Could not delete invoice: ' . $InvoiceService->lastError());
        }
    }

    public function updateInvoice() 
    {
        $InvoiceService = new QuickBooks_IPP_Service_Invoice();
        $invoices = $InvoiceService->query($this->Context, $this->Realm, "SELECT * FROM Invoice WHERE Id = '$this->id' ");
        if(!empty($invoices)) 
        {
            $Invoice = $invoices[0];
            if($this->txnDate != null) 
            {
                $Invoice->setTxnDate($this->txnDate);
            }

            if($this->address != null) 
            {
                $SenderAddress = $Invoice->getBillEmail();
                $SenderAddress->setAddress($this->address);
            }
            if ($resp = $InvoiceService->update($this->Context, $this->Realm, $Invoice->getId(), $Invoice))
            {
                return [true];
            }
            else
            {
                return('&nbsp; ' . $this->InvoiceService->lastError() . '<br>');
            }
        }
        else 
        {
            return ("Could not find invoice " . $this->id . " to update");
        }
    }

    public function addLineToInvoice()
    {
        $invoices = $this->InvoiceService->query($this->Context, $this->Realm, "SELECT * FROM Invoice WHERE Id = '$this->id' ");
        if(!empty($invoices)) 
        {
            $Invoice = $invoices[0];

            $Line = new QuickBooks_IPP_Object_Line();
            $Line->setDetailType($this->detailType);
            $Line->setAmount($this->unitPrice * $this->qty);
            $Line->setDescription($this->description);

            $SalesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
            $SalesItemLineDetail->setItemRef($this->itemRef);
            $SalesItemLineDetail->setUnitPrice($this->unitPrice);
            $SalesItemLineDetail->setQty($this->qty);

            $Line->addSalesItemLineDetail($SalesItemLineDetail);
            
            $Invoice->addLine($Line);
            $Line = $Invoice->getLine(1);

            if ($resp = $this->InvoiceService->update($this->Context, $this->Realm, $Invoice->getId(), $Invoice))
            {
                return true;
            }
            else
            {
                return('&nbsp; ' . $this->InvoiceService->lastError() . '<br>');
            }
        }
        else 
        {
            return ("Could not find invoice " . $this->id . " to update");
        }
    }

    public function removeLineFromInvoice($id, $lineNoToRemove)
    {
        $invoices = $InvoiceService->query($this->Context, $this->Realm, "SELECT * FROM Invoice WHERE Id = '$id' ");
        if(!empty($invoices)) 
        {
            $Invoice = $invoices[0];
            $Invoice->unsetLine($lineNoToRemove-1);
            if ($resp = $InvoiceService->update($this->Context, $this->Realm, $Invoice->getId(), $Invoice))
            {
                return true;
            }
            else
            {
                return('&nbsp; ' . $InvoiceService->lastError() . '<br>');
            }
        }
        else 
        {
            return ("Could not find invoice " . $id . " to update");
        }
    }


    // not sure what this function does
    public function sendInvoice($objOfInvoice, $Context, $realm) 
    {
        $invoices = $this->InvoiceService->query($this->Context, $this->Realm, "SELECT * FROM Invoice WHERE Id = '$id' ");
        if(!empty($invoices)) 
        {
            $Invoice = $invoices[0];
            if ($resp = $this->InvoiceService->send($this->Context, $this->Realm, $Invoice->getId()));
            {
                 return(["Id" => $Invoice->getId()]);
            }
        }
        else 
        { 
            return(["Error" => $this->InvoiceService->lastError()]);
        }
    }

}

