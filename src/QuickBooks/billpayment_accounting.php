<?php

class BillPayment
{
    public $id;
    public $txnDate;
    public $vendorRef;
    public $payType; // valid types are 'Check', 'CreditCard'
    public $checkPayment; // req'd if $payType = 'Check'
    public $creditCardPayment; // req'd if $payType = 'CreditCard'
    public $totalAmt;
    public $linkedTxn;
    public $lineItems = []; // array containing objects for each line item associated with the bill payment

    public function __construct($Context, $Realm)
    {
        $this->Context = $Context;
        $this->Realm = $Realm;
        $this->BillService = new QuickBooks_IPP_Service_BillPayment();
    }

    public function setAttributes($attributes)
    {
        foreach ($attributes as $key => $value) 
        {
            $this->{$key} = $value;
        }
    }

    public function showBill($poId) {

        $bills = $this->BillService->query($this->Context, $this->Realm, "SELECT * FROM BillPayment where id='" . $poId . "'");
        $arrayToSend = array();

        foreach ($bills as $bill)
        {

            $arrayToSend[] = array(

                "docNumber" => $bill->getDocNumber(),
                "totalAmt" => $bill->getTotalAmt(),
                "poDate" =>$bill->getTxnDate(),
                "description" => $bill->getLine(0)->getDescription(),
                "id" => QuickBooks_IPP_IDS::usableIDType($bill->getId())
            );
        }
        return($arrayToSend);

    }

public function addBill()
    {

        $Bill = new QuickBooks_IPP_Object_BillPayment();
        $this->creditCardPayment = new QuickBooks_IPP_Object_CreditCardPayment();
        $this->checkPayment = new QuickBooks_IPP_Object_CheckPayment();
        $this->checkPayment->setBankAccountRef('35'); 
        $this->creditCardPayment->setBankAccountRef('35');
        $Bill->setTxnDate($this->txnDate);
        $Bill->setVendorRef($this->vendorRef);
        $Bill->setPayType($this->payType);
        $Bill->setTotalAmt($this->totalAmt);
        $Bill->setBankAccountRef('35');
        if($this->payType == "Check")
        {
            $Bill->setCheckPayment($this->checkPayment);
        }
        else
        {
            $Bill->setCreditCardPayment($this->creditCardPayment);
        }
        $Line = $this->addLineItemsToNewBillPayment();
        $Bill->addLine($Line);
        if ($resp = $this->BillService->add($this->Context, $this->Realm, $Bill)) 
        {
            return(["Success" => QuickBooks_IPP_IDS::usableIDType($resp)]);
        } 
        else 
        {
            return(["Error" => $BillService->lastError()]);
        }
    }
    public function addLineItemsToNewBillPayment() 
    {
        $Line = new QuickBooks_IPP_Object_Line();
        $Line->setAmount($this->totalAmt);
        $linkedTxn = new QuickBooks_IPP_Object_LinkedTxn();
        $linkedTxn->setTxnId($this->linkedTxn);
        $linkedTxn->setTxnType("Bill");
        $Line->setLinkedTxn($linkedTxn);

        return $Line;
    }

    public function updateBill($item, $Context, $realm) 
    {
        $bills = $BillService->query($Context, $realm, "SELECT * FROM BillPayment WHERE Id = '$this->id' ");
        if(!empty($bills)) 
        {
            $bill = $bills[0];
            if($this->txnDate != null) 
            {
                $bill->setTxnDate($this->txnDate);
            }
            if ($resp = $BillService->update($this->Context, $this->Realm, $bill->getId(), $bill))
            {
                return true;
            }
            else
            {
                return('&nbsp; ' . $this->BillService->lastError() . '<br>');
            }
        }
        else 
        {
            return ("Could not find bill ".$this->id. " to update");
        }
    }

    public function addLineToBill($item)
    {
        $bills = $this->BillService->query($this->Context, $this->Realm, "SELECT * FROM BillPayment WHERE Id = '$this->id' ");
        if(!empty($bills)) 
        {
            $bill = $bills[0];
            $Line = new QuickBooks_IPP_Object_Line();
            $Line->setAmount($item->amount);
            $Line->setLinkedTxn(["TxnId" => $item->linkedTxn, "TxnType" => "Bill"]);
        
            $bill->addLine($Line);
            if ($resp = $this->BillService->update($this->Context, $this->realm, $bill->getId(), $bill))
            {
                return true;
            }
            else
            {
                return('&nbsp; ' . $this->BillService->lastError() . '<br>');
            }
        }
        else
        {
            return ("Could not find bill ".$this->id. " to update");
        }
    }

    public function showBillPayments()
    {
        return $this->BillService->query($this->Context, $this->Realm, "SELECT * FROM BillPayment STARTPOSITION 1 MAXRESULTS 10");
    }
    public function showBillPaymentById($id)
    {
        return $this->BillService->query($this->Context, $this->Realm, "SELECT * FROM BillPayment WHERE Id = '" . $id . "'");
    }
}
