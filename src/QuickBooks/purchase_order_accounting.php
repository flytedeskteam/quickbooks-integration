<?php

class Purchase_Order
{
    public $id;
    public $docNumber;
    public $txnDate;
    public $detailType;
    public $description;
    public $itemRef;
    public $unitPrice;
    public $qty;
    public $customerRef;
    public $vendorRef;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setAttributes($docNumber, $txnDate, $detailType, $description, $itemRef, $unitPrice, $qty, $customerRef, $vendorRef)
    {
        $this->docNumber = $docNumber;
        $this->txnDate = $txnDate;
        $this->detailType = $detailType;
        $this->description = $description;
        $this->itemRef = $itemRef;
        $this->unitPrice = $unitPrice;
        $this->qty = $qty;
        $this->customerRef = $customerRef;
        $this->vendorRef = $vendorRef;
    }
}

Class Purchase_Order_Query
{
    public function showPurchaseOrder($poId, $Context, $realm) {

        $PurchaseOrderService = new QuickBooks_IPP_Service_PurchaseOrder();

        $pos = $PurchaseOrderService->query($Context, $realm, "SELECT * FROM PurchaseOrder where id='" . $poId . "'");
        $arrayToSend = array();

        foreach ($pos as $PurchaseOrder)
        {

            $arrayToSend[] = array(

                "docNumber" => $PurchaseOrder->getDocNumber(),
                "totalAmt" => $PurchaseOrder->getTotalAmt(),
                "poDate" =>$PurchaseOrder->getTxnDate(),
                "description" => $PurchaseOrder->getLine(0)->getDescription(),
                "id" => QuickBooks_IPP_IDS::usableIDType($PurchaseOrder->getId())
            );
        }
        return($arrayToSend);

    }
}

Class Purchase_Order_Add
{
    public function addPurchaseOrder($objOfPurchaseOrder, $Context, $realm)
    {
        $PurchaseOrderService = new QuickBooks_IPP_Service_PurchaseOrder();
        $PurchaseOrder = new QuickBooks_IPP_Object_PurchaseOrder();
        $PurchaseOrder->setDocNumber($objOfPurchaseOrder->docNumber . mt_rand(10000, 99999));
        $PurchaseOrder->setTxnDate($objOfPurchaseOrder->txnDate);
        $Line = new QuickBooks_IPP_Object_Line();
        $Line->setDetailType($objOfPurchaseOrder->detailType);
        $Line->setAmount($objOfPurchaseOrder->unitPrice * $objOfPurchaseOrder->qty);
        $Line->setDescription($objOfPurchaseOrder->description);
        $SalesItemLineDetail = new QuickBooks_IPP_Object_ItemBasedExpenseLineDetail();
        $SalesItemLineDetail->setCustomerRef($objOfPurchaseOrder->customerRef);
        $SalesItemLineDetail->setItemRef($objOfPurchaseOrder->itemRef);
        $SalesItemLineDetail->setUnitPrice($objOfPurchaseOrder->unitPrice);
        $SalesItemLineDetail->setQty($objOfPurchaseOrder->qty);
        $Line->addItemBasedExpenseLineDetail($SalesItemLineDetail);
        $PurchaseOrder->addLine($Line);
        $PurchaseOrder->setVendorRef($objOfPurchaseOrder->vendorRef);

        if ($resp = $PurchaseOrderService->add($Context, $realm, $PurchaseOrder)) 
        {
            return(QuickBooks_IPP_IDS::usableIDType($resp));
        } 
        else 
        {
            return($PurchaseOrderService->lastError());
        }
    }
}

Class Purchase_Order_Delete 
{
    public function deletePurchaseOrder($objectOfPurchaseOrder, $Context, $realm)
    {
        $PurchaseOrderService = new QuickBooks_IPP_Service_PurchaseOrder();
        $the_purchase_order_to_delete = '{'.'-'.$objectOfPurchaseOrder->id.'}';
        $retr = $PurchaseOrderService->delete($Context, $realm, $the_purchase_order_to_delete);
        $error = $PurchaseOrderService->lastError();
        if(empty($error))
        {
            return true;
        }
        else
        {
            return('Could not delete purchase order: ' . $PurchaseOrderService->lastError());
        }
    }
}


Class Purchase_Order_Update 
{
    public function updatePurchaseOrder($objOfPurchaseOrder, $Context, $realm) 
    {
        $PurchaseOrderService = new QuickBooks_IPP_Service_PurchaseOrder();
        $pos = $PurchaseOrderService->query($Context, $realm, "SELECT * FROM PurchaseOrder WHERE Id = '$objOfPurchaseOrder->id' ");
        if(!empty($pos)) 
        {
            $PO = $pos[0];
            if($objOfPurchaseOrder->txnDate != null) 
            {
                $PO->setTxnDate($objOfPurchaseOrder->txnDate);
            }
            if ($resp = $PurchaseOrderService->update($Context, $realm, $PO->getId(), $PO))
            {
                return true;
            }
            else
            {
                return('&nbsp; ' . $PurchaseOrderService->lastError() . '<br>');
            }
        }
        else 
        {
            return ("Could not find purchase_order ".$objOfPurchaseOrder->id. " to update");
        }
    }
}

Class Purchase_Order_Line_Add 
{
    public function addLineToPurchaseOrder($objOfPurchaseOrder, $Context, $realm)
    {
        $PurchaseOrderService = new QuickBooks_IPP_Service_PurchaseOrder();
        $pos = $PurchaseOrderService->query($Context, $realm, "SELECT * FROM PurchaseOrder WHERE Id = '$objOfPurchaseOrder->id' ");
        if(!empty($pos)) 
        {
            $PO = $pos[0];
            $Line = new QuickBooks_IPP_Object_Line();
            $Line->setDetailType($objOfPurchaseOrder->detailType);
            $Line->setAmount($objOfPurchaseOrder->unitPrice * $objOfPurchaseOrder->qty);
            $Line->setDescription($objOfPurchaseOrder->description);
            $SalesItemLineDetail = new QuickBooks_IPP_Object_ItemBasedExpenseLineDetail();
            $SalesItemLineDetail->setCustomerRef($objOfPurchaseOrder->customerRef);
            $SalesItemLineDetail->setItemRef($objOfPurchaseOrder->itemRef);
            $SalesItemLineDetail->setUnitPrice($objOfPurchaseOrder->unitPrice);
            $SalesItemLineDetail->setQty($objOfPurchaseOrder->qty);
            $Line->addSalesItemLineDetail($SalesItemLineDetail);
            $PO->addLine($Line);

            if ($resp = $PurchaseOrderService->update($Context, $realm, $PO->getId(), $PO))
            {
                return true;
            }
            else
            {
                return('&nbsp; ' . $PurchaseOrderService->lastError() . '<br>');
            }
        }
        else
        {
            return ("Could not find purchase_order ".$objOfPurchaseOrder->id. " to update");
        }
    }
}

Class Purchase_Order_Line_Remove 
{
    public function removeLineFromPurchaseOrder($objOfPurchaseOrder, $lineNoToRemove, $Context, $realm)
    {
        // Get the existing invoice first (you need the latest SyncToken value)
        $PurchaseOrderService = new QuickBooks_IPP_Service_PurchaseOrder();
        $pos = $PurchaseOrderService->query($Context, $realm, "SELECT * FROM PurchaseOrder WHERE Id = '$objOfPurchaseOrder->id' ");
        if(!empty($pos)) 
        {
            $PO = $pos[0];
            $PO->unsetLine($lineNoToRemove-1);
            if ($resp = $PurchaseOrderService->update($Context, $realm, $PO->getId(), $PO))
            {
                return true;
            }
            else
            {
                return('&nbsp; ' . $PurchaseOrderService->lastError() . '<br>');
            }
        }
        else 
        {
            return ("Could not find purchase_order ".$objOfPurchaseOrder->id. " to update");
        }
    }

    public function removeInitialLineFromPurchaseOrder($objOfPurchaseOrder, $Context, $realm)
    {
        // remove the "Initial" line from the PO.
        $PurchaseOrderService = new QuickBooks_IPP_Service_PurchaseOrder();
        $pos = $PurchaseOrderService->query($Context, $realm, "SELECT * FROM PurchaseOrder where id='$objOfPurchaseOrder->id'");
        foreach($pos as $p)
        {
            if($p->getLine(0)->getDescription() == "Initial")
            {
                $p->unsetLine(0);
                if ($resp = $PurchaseOrderService->update($Context, $realm, $p->getId(), $p))
                {
                    return true;
                }
                else
                {
                    return('&nbsp; ' . $PurchaseOrderService->lastError() . '<br>');
                }
            }
        }
    }
}

