<?php


Class Employee_Query
{
    public function showEmployee($Context, $realm)
    {

        $EmployeeService = new QuickBooks_IPP_Service_Employee();
        $employees = $EmployeeService->query($Context, $realm, "SELECT * FROM Employee ");
        $arrayToSend = array();


        foreach ($employees as $Employee) {

           $arrayToSend[] = array(
               "id" => QuickBooks_IPP_IDS::usableIDType($Employee->getId()),
               "name" => $Employee->getGivenName(),
               "family_name" => $Employee->getFamilyName()
           );

        }

        return($arrayToSend);

    }
}




