<?php

class Bill
{
    public $lineItems = [];

    protected $BillService;

    public function __construct($Context, $Realm){
        $this->Context = $Context;
        $this->Realm = $Realm;
        $this->BillService = new QuickBooks_IPP_Service_Bill();
    }
    public function setAttributes($attributes)
    {
        foreach ($attributes as $key => $value) 
        {
            $this->{$key} = $value;
        }
    }

    public function addLineToBill($objOfBill)
    {
        $bills = $this->BillService->query($this->Context, $this->Realm, "SELECT * FROM Bill WHERE Id = '$objOfBill->id' ");
        if(!empty($bills)) 
        {
            $bill = $bills[0];
            $Line = new QuickBooks_IPP_Object_Line();
            $Line->setDetailType($objOfBill->detailType);
            $Line->setAmount($objOfBill->unitPrice * $objOfBill->qty);
            $Line->setDescription($objOfBill->description);
            $SalesItemLineDetail = new QuickBooks_IPP_Object_ItemBasedExpenseLineDetail();
            $SalesItemLineDetail->setCustomerRef($objOfBill->customerRef);
            $SalesItemLineDetail->setItemRef($objOfBill->itemRef);
            $SalesItemLineDetail->setUnitPrice($objOfBill->unitPrice);
            $SalesItemLineDetail->setQty($objOfBill->qty);
            $Line->addSalesItemLineDetail($SalesItemLineDetail);
            $bill->addLine($Line);

            if ($resp = $this->BillService->update($this->Context, $this->Realm, $bill->getId(), $bill))
            {
                return true;
            }
            else
            {
                return('&nbsp; ' . $this->BillService->lastError() . '<br>');
            }
        }
        else
        {
            return ("Could not find bill ".$objOfBill->id. " to update");
        }
    }
    public function showBill($billId) 
    {

        return $this->BillService->query($this->Context, $this->Realm, "SELECT * FROM Bill where id='" . $billId . "'");
    }

    public function showBills($billId) 
    {
        return $this->BillService->query($this->Context, $this->Realm, "SELECT * FROM Bill");
    }
    public function showBillsByVendor($vendorId)
    {
        return $this->BillService->query($this->Context, $this->Realm, "SELECT * FROM Bill where vendorRef='" . $vendorId . "'");
    }

    public function showBillsByVendorDate($vendorId, $startDate, $endDate)
    {
        $query = "SELECT * FROM Bill where vendorRef='" . $vendorId . "' and TxnDate > '" . $startDate . "' and TxnDate < '" . $endDate . "'";
        return $this->BillService->query($this->Context, $this->Realm, $query);
    }
    public function addBill()
    {
        $Bill = new QuickBooks_IPP_Object_Bill();
        $Bill->setDocNumber($this->docNumber);
        $Bill->setTxnDate($this->txnDate);
        $Bill->setSalesTermRef('8');


        foreach ($this->lineItems as $key => $value) 
        {
            $Line = $this->addLineItemsToNewBill($value);
            $Bill->addLine($Line);  
        }
        $Bill->setVendorRef($this->vendorRef);
        if ($resp = $this->BillService->add($this->Context, $this->Realm, $Bill)) 
        {
            return(["Id" => QuickBooks_IPP_IDS::usableIDType($resp)]);
        } 
        else 
        {
            return(["Error" => $this->BillService->lastError()]);
        }
    }

    public function addLineItemsToNewBill($item) 
    {
        $Line = new QuickBooks_IPP_Object_Line();
        $SalesItemLineDetail = new QuickBooks_IPP_Object_ItemBasedExpenseLineDetail();
        $description = $item->campaign_name . ' - ' . $item->display_name . ' ' . $item->attribute . ' - ' . $item->asset_date;
        if($item->make_good_id > 0) {
            $description .= ' MAKE GOOD';
        }
        if($item->qty > 0) {
            $Line->setAmount($item->cost_payout * $item->qty);
            $SalesItemLineDetail->setQty($item->qty);
        } else {
            //temp - deal with some assets having qty of 0
            $Line->setAmount($item->cost_payout);
            $SalesItemLineDetail->setQty('1');
        }

        $Line->setDetailType('ItemBasedExpenseLineDetail');
        $Line->setDescription($description);
        $SalesItemLineDetail->setCustomerRef($item->buyer_accounting_id);
        $SalesItemLineDetail->setItemRef($item->itemRef);
        $SalesItemLineDetail->setUnitPrice($item->cost_payout);
        $Line->addItemBasedExpenseLineDetail($SalesItemLineDetail);

        return $Line;
    }
    public function updateBill($item) 
    {
        $bills = $this->BillService->query($this->Context, $this->Realm, "SELECT * FROM Bill WHERE Id = '$this->id' ");
        if(!empty($bills)) 
        {
            $bill = $bills[0];
            if($this->txnDate != null) 
            {
                $bill->setTxnDate($this->txnDate);
            }
            if ($resp = $this->BillService->update($this->Context, $this->Realm, $bill->getId(), $bill))
            {
                return true;
            }
            else
            {
                return('&nbsp; ' . $this->BillService->lastError() . '<br>');
            }
        }
        else 
        {
            return ("Could not find bill ".$this->id. " to update");
        }
    }
}