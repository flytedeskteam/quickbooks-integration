<?php

Class Payment
{
    public $paymentRefNum;
    public $txnDate;
    public $totalAmt;
    public $txnId;
    public $txnType;
    public $customerRef;

    public function setAttributes($paymentRefNum, $txnDate, $totalAmt, $txnId, $txnType, $customerRef)
    {
        $this->paymentRefNum = $paymentRefNum;
        $this->txnDate = $txnDate;
        $this->totalAmt = $totalAmt;
        $this->txnId = $txnId;
        $this->txnType = $txnType;
        $this->customerRef = $customerRef;
    }

}

Class Payment_Query
{
    public function showPayment($Context, $realm)
    {
        $PaymentService = new QuickBooks_IPP_Service_Payment();
        return $PaymentService->query($Context, $realm, "SELECT * FROM Payment STARTPOSITION 1 MAXRESULTS 10");
    }
    public function showPaymentsById($paymentId, $Context, $realm)
    {
        $PaymentService = new QuickBooks_IPP_Service_Payment();
        return $PaymentService->query($Context, $realm, "SELECT * FROM Payment where id='" . $paymentId . "'");
    }    
}

Class Payment_Add
{
    public function addPayment($objOfPayment, $Context, $realm)
    {

        $PaymentService = new QuickBooks_IPP_Service_Payment();

// Create payment object
        $Payment = new QuickBooks_IPP_Object_Payment();

        $Payment->setPaymentRefNum($objOfPayment->paymentRefNum);
        $Payment->setTxnDate($objOfPayment->txnDate);
        $Payment->setTotalAmt($objOfPayment->totalAmt);

// Create line for payment (this details what it's applied to)
        $Line = new QuickBooks_IPP_Object_Line();
        $Line->setAmount($objOfPayment->totalAmt);

// The line has a LinkedTxn node which links to the actual invoice
        $LinkedTxn = new QuickBooks_IPP_Object_LinkedTxn();
//        $LinkedTxn->setTxnId('{-84}');
        $LinkedTxn->setTxnId('{-' . $objOfPayment->txnId . '}');
        $LinkedTxn->setTxnType($objOfPayment->txnType);

        $Line->setLinkedTxn($LinkedTxn);

        $Payment->addLine($Line);

        $Payment->setCustomerRef('{-' . $objOfPayment->customerRef . '}');
//        $Payment->setCustomerRef('{-67}');

// Send payment to QBO
        if ($resp = $PaymentService->add($Context, $realm, $Payment)) {
//            print('Our new Payment ID is: [' . $resp . ']');

            return(QuickBooks_IPP_IDS::usableIDType($resp));
        } else {
            return($PaymentService->lastError());
        }
    }
}


