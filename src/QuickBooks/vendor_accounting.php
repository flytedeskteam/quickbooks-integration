<?php

class Vendor
{
    public $id;
    public $title;
    public $givenName;
    public $middleName;
    public $familyName;
    public $displayName;
    public $primaryPhoneNumber;
    public $mobileNumber;
    public $faxNumber;
    public $billAddrLine1;
    public $billAddrLine2;
    public $billAddrCity;
    public $billAddrCountrySubDivisionCode;
    public $billAddrPostalCode;
    public $primaryEmailAddress;
    public $companyName;
    public $acctNum;

    public $VendorService;

    public function __construct($Context, $Realm)
    {
        parent::__construct($Context, $Realm);
        $this->VendorService = new QuickBooks_IPP_Service_Vendor();
    }

    // is this function being used
     public function showVendor()
    {
        $vendors = $this->VendorService->query($this->Context, $this->Realm, "SELECT * FROM Vendor");
        $arrayToSend = array();
        foreach ($vendors as $Vendor) {
            $arrayToSend[] = array(
                "id" => QuickBooks_IPP_IDS::usableIDType($Vendor->getId()),
                "displayName" => $Vendor->getDisplayName()
            );
        }
        return($arrayToSend);
    }
    public function showAllVendors($start)
    {
        $vendors = $this->VendorService->query($this->Context, $this->Realm, "SELECT * FROM Vendor startposition ". $start);
        $arrayToSend = array();
        foreach ($vendors as $Vendor) {
            $arrayToSend[] = array(
                "id" => QuickBooks_IPP_IDS::usableIDType($Vendor->getId()),
                "displayName" => $Vendor->getDisplayName()
            );
        }
        return($arrayToSend);
    }

    public function showSingleVendor($id)
    {
        $vendors = $this->VendorService->query($this->Context, $this->Realm, "SELECT * FROM Vendor where id='" . $id . "'");
        $arrayToSend = array();
        foreach ($vendors as $Vendor) 
        {
            $arrayToSend[] = array(
                "id" => QuickBooks_IPP_IDS::usableIDType($Vendor->getId()),
                "displayName" => $Vendor->getDisplayName()
            );
        }
        return($arrayToSend);
    }
    
    public function showVendorByName($name)
    {
        $vendors = $VendorService->query($this->Context, $this->Realm, "SELECT * FROM Vendor where DisplayName='" . $name . "'");
        if(!isset($vendors[0]))
        {
            // no existing vendor. Return null.
            return null;
        }      
        else
        {
            $arrayToSend = array();
            foreach ($vendors as $Vendor) 
            {
                $arrayToSend[] = array(
                    "id" => QuickBooks_IPP_IDS::usableIDType($Vendor->getId()),
                    "displayName" => $Vendor->getDisplayName()
                );
            }
            return($arrayToSend);
        }
    }

    public function addVendor()
    {
        $Vendor = new QuickBooks_IPP_Object_Vendor();
        $Vendor->setTitle($this->title);
        $Vendor->setGivenName($this->givenName);
        $Vendor->setMiddleName($this->middleName);
        $Vendor->setFamilyName($this->familyName);
        $Vendor->setDisplayName($this->displayName);
        $Vendor->setCompanyName($this->companyName);
        $Vendor->setAcctNum(uniqid('vendor_'));

        // Phone #
        $PrimaryPhone = new QuickBooks_IPP_Object_PrimaryPhone();
        $PrimaryPhone->setFreeFormNumber($this->primaryPhoneNumber);
        $Vendor->setPrimaryPhone($PrimaryPhone);

        // Mobile #
        $Mobile = new QuickBooks_IPP_Object_Mobile();
        $Mobile->setFreeFormNumber($this->mobileNumber);
        $Vendor->setMobile($Mobile);

        // Fax #
        $Fax = new QuickBooks_IPP_Object_Fax();
        $Fax->setFreeFormNumber($this->faxNumber);
        $Vendor->setFax($Fax);

        // Bill address
        $BillAddr = new QuickBooks_IPP_Object_BillAddr();
        $BillAddr->setLine1($this->billAddrLine1);
        $BillAddr->setLine2($this->billAddrLine2);
        $BillAddr->setCity($this->billAddrCity);
        $BillAddr->setCountrySubDivisionCode($this->billAddrCountrySubDivisionCode);
        $BillAddr->setPostalCode($this->billAddrPostalCode);
        $Vendor->setBillAddr($BillAddr);

        // Email
        $PrimaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
        $PrimaryEmailAddr->setAddress($this->primaryEmailAddress);
        $Vendor->setPrimaryEmailAddr($PrimaryEmailAddr);

        if ($resp = $VendorService->add($this->Context, $this->Realm, $Vendor)) 
        {
            return(QuickBooks_IPP_IDS::usableIDType($resp));
        } 
        else 
        {
            return($VendorService->lastError($this->Context));
        }
    }

    public function updateVendor()
    {
        // Get the existing customer first (you need the latest SyncToken value)
        $vendors = $VendorService->query($this->Context, $realm, "SELECT * FROM Vendor WHERE Id = '$this->id' ");
        if(!empty($vendors)) {
        $Vendor = $vendors[0];

        // Change something

            if($this->title != null) {
                $Vendor->setTitle($this->title);
            }

            if($this->givenName != null) {
                $Vendor->setGivenName($this->givenName);
            }

            if($this->middleName != null) {
                $Vendor->setMiddleName($this->middleName);
            }

            if($this->familyName != null) {
                $Vendor->setFamilyName($this->familyName);
            }

            if($this->displayName != null) {
                $Vendor->setDisplayName($this->displayName);
            }

            if($this->primaryPhoneNumber != null) {
                $PrimaryPhone = $Vendor->getPrimaryPhone();
                if($PrimaryPhone == null)
                {
                    $PrimaryPhone = new QuickBooks_IPP_Object_PrimaryPhone();
                } 
                $PrimaryPhone->setFreeFormNumber($this->primaryPhoneNumber);
                $Vendor->setPrimaryPhone($PrimaryPhone);
            }

            if($this->mobileNumber != null) {
                $Mobile = $Vendor->getMobile();
                if($Mobile == null)
                {
                    $Mobile = new QuickBooks_IPP_Object_Mobile();
                }
                $Mobile->setFreeFormNumber($this->mobileNumber);
                $Vendor->setMobile($Mobile);
            }

            if($this->faxNumber != null) {
                $Fax = $Vendor->getFax();
                if($Fax == null)
                {
                    $Fax = new QuickBooks_IPP_Object_Fax(); 
                }
                $Fax->setFreeFormNumber($this->faxNumber);
            }

            // Update Vendor Address
            $BillAddr = $Vendor->getBillAddr();
            if($BillAddr == null)
            {
                $BillAddr = new QuickBooks_IPP_Object_BillAddr();   
            }

            if($this->billAddrLine1 != null) {
                $BillAddr->setLine1($this->billAddrLine1);
            }

            if($this->billAddrLine2 != null) {
                $BillAddr->setLine2($this->billAddrLine2);
            }

            if($this->billAddrCity != null) {
                $BillAddr->setCity($this->billAddrCity);
            }

            if($this->billAddrCountrySubDivisionCode != null) {
                $BillAddr->setCountrySubDivisionCode($this->billAddrCountrySubDivisionCode);
            }

            if($this->billAddrPostalCode != null) {
                $BillAddr->setPostalCode($this->billAddrPostalCode);
            }
            $Vendor->setBillAddr($BillAddr);

            // Update their email address too
            if($this->primaryEmailAddress != null) {
                $PrimaryEmailAddr = $Vendor->getPrimaryEmailAddr();
                if($PrimaryEmailAddr == null)
                {
                    $PrimaryEmailAddr = new QuickBooks_IPP_Object_PrimaryEmailAddr();
                }
                $PrimaryEmailAddr->setAddress($this->primaryEmailAddress);
                $Vendor->setPrimaryEmailAddr($PrimaryEmailAddr);
            }


        if ($VendorService->update($this->Context, $this->Realm, $Vendor->getId(), $Vendor))
        {
            return true;
        }
        else
        {
            return('&nbsp; Error: ' . $VendorService->lastError($this->Context));
        }
        }else return ("Could not find vendor ".$this->id. " to update");

    }

    public function deleteVendor($id) 
    {
        // Get the existing customer first (you need the latest SyncToken value)
        $vendors = $this->VendorService->query($this->Context, $this->Realm, "SELECT * FROM Vendor WHERE Id = '$id' ");
        if(!empty($vendors)) 
        {
            $Vendor = $vendors[0];
            $array = array();
            $array[0] = "false";
            $Vendor->setActive($array);

            if ($VendorService->update($this->Context, $this->Realm, $Vendor->getId(), $Vendor))
            {
                return true;
            }
            else
            {
                return('&nbsp; Error: ' . $VendorService->lastError($this->Context));
            }
        }
        else 
        {
            return ("Could not find vendor ".$id. " to delete");
        }
    }

}

