<?php

class Credit_Memo
{
    public $id;
    public $txnDate;
    public $customerRef;
    public $vendorRef;
    public $dueDate;
    public $syncToken;
    public $docNumber;
    public $detailType;
    public $unitPrice;
    public $qty;
    public $description;
    public $lineItems = [];

    public function __construct($Context, $Realm)
    {
        $this->Context = $Context;
        $this->Realm = $Realm;
        $this->CreditMemoService = new QuickBooks_IPP_Service_CreditMemo();
    }

    public function setAttributes($attributes)
    {
        foreach ($attributes as $key => $value)
        {
            $this->{$key} = $value;
        }
    }

    public function setLineItems($lines)
    {
        array_push($this->lineItems, $lines);
    }

    public function addCreditMemo()
    {
        $CreditMemo = new QuickBooks_IPP_Object_CreditMemo();
        $CreditMemo->setDocNumber($this->docNumber);
        $CreditMemo->setTxnDate($this->txnDate);
        $Line = $this->addLineItemsToCreditMemo($this->lineItems);

        $CreditMemo->addLine($Line);
        $CreditMemo->setVendorRef($this->vendorRef);
        $CreditMemo->setCustomerRef($this->customerRef);
        if ($resp = $this->CreditMemoService->add($this->Context, $this->Realm, $CreditMemo)) 
        {
            return(["Id" => QuickBooks_IPP_IDS::usableIDType($resp)]);
        } 
        else 
        {
            return(["Error" => $this->CreditMemoService->lastError()]);
        }
    }
    public function addLineItemsToCreditMemo($item)
    {
        $Line = new QuickBooks_IPP_Object_Line();
        $Line->setDetailType($item->detailType);
        $Line->setAmount($item->unitPrice * $item->qty);
        $Line->setDescription($item->description);
        $SalesItemLineDetail = new QuickBooks_IPP_Object_SalesItemLineDetail();
        $SalesItemLineDetail->setCustomerRef($item->customerRef);
        $SalesItemLineDetail->setItemRef($item->itemRef);
        $SalesItemLineDetail->setUnitPrice($item->unitPrice);
        $SalesItemLineDetail->setQty($item->qty);
        $Line->addSalesItemLineDetail($SalesItemLineDetail);

        return $Line;
    }

}
