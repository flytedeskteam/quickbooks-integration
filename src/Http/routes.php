<?php
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => env('SECURITY_PREFIX')], function () {

	Route::get('customerCreate', 'Flytedesk\Accounting\Http\AccountingExampleController@getCustomerCreate');
    Route::get('customerCount', 'Flytedesk\Accounting\Http\AccountingExampleController@getCustomerCount');
	Route::get('customerUpdate', 'Flytedesk\Accounting\Http\AccountingExampleController@getCustomerUpdate');
	Route::get('customerRead', 'Flytedesk\Accounting\Http\AccountingExampleController@getCustomerRead');
	Route::get('customerDelete', 'Flytedesk\Accounting\Http\AccountingExampleController@getCustomerDelete');
	Route::get('customerPaymentAccept', 'Flytedesk\Accounting\Http\AccountingExampleController@getCustomerPaymentAccept');

	Route::get('vendorCreate', 'Flytedesk\Accounting\Http\AccountingExampleController@getVendorCreate');
	Route::get('vendorUpdate', 'Flytedesk\Accounting\Http\AccountingExampleController@getVendorUpdate');
	Route::get('vendorRead', 'Flytedesk\Accounting\Http\AccountingExampleController@getVendorRead');
	Route::get('vendorDelete', 'Flytedesk\Accounting\Http\AccountingExampleController@getVendorDelete');

	Route::get('invoiceCreate', 'Flytedesk\Accounting\Http\AccountingExampleController@getInvoiceCreate');
	Route::get('invoiceUpdate', 'Flytedesk\Accounting\Http\AccountingExampleController@getInvoiceUpdate');
	Route::get('invoiceRead', 'Flytedesk\Accounting\Http\AccountingExampleController@getInvoiceRead');
	Route::get('invoiceDelete', 'Flytedesk\Accounting\Http\AccountingExampleController@getInvoiceDelete');
	Route::get('invoiceLineAdd', 'Flytedesk\Accounting\Http\AccountingExampleController@getInvoiceLineAdd');
	Route::get('invoiceLineRemove', 'Flytedesk\Accounting\Http\AccountingExampleController@getInvoiceLineRemove');
	Route::get('invoiceLineSend', 'Flytedesk\Accounting\Http\AccountingExampleController@getInvoiceSend');
	Route::get('invoicePaymentAccept', 'Flytedesk\Accounting\Http\AccountingExampleController@getInvoicePaymentAccept');

	Route::get('billCreate', 'Flytedesk\Accounting\Http\AccountingExampleController@getBillCreate');
	Route::get('creditMemoCreate', 'Flytedesk\Accounting\Http\AccountingExampleController@getCreditMemoCreate');
	Route::get('vendorCreditCreate', 'Flytedesk\Accounting\Http\AccountingExampleController@getVendorCreditCreate');

	Route::get('POCreate', 'Flytedesk\Accounting\Http\AccountingExampleController@getPOCreate');
	Route::get('POUpdate', 'Flytedesk\Accounting\Http\AccountingExampleController@getPOUpdate');
	Route::get('PORead', 'Flytedesk\Accounting\Http\AccountingExampleController@getPORead');
	Route::get('PODelete', 'Flytedesk\Accounting\Http\AccountingExampleController@getPODelete');
	Route::get('POLineAdd', 'Flytedesk\Accounting\Http\AccountingExampleController@getPOLineAdd');
	Route::get('POLineRemove', 'Flytedesk\Accounting\Http\AccountingExampleController@getPOLineRemove');
	
    Route::get('paymentRead', 'Flytedesk\Accounting\Http\AccountingExampleController@getPaymentRead');
    Route::get('employeeRead', 'Flytedesk\Accounting\Http\AccountingExampleController@getEmployeeRead');
    Route::get('reconnect', 'Flytedesk\Accounting\Http\AccountingExampleController@reconnect');
    Route::get('logoff', 'Flytedesk\Accounting\Http\AccountingExampleController@logout');
    Route::get('diagnose', 'Flytedesk\Accounting\Http\AccountingExampleController@diagnostic');

	Route::get('package', 'Flytedesk\Accounting\Http\AccountingExampleController@welcomePage');

    Route::get('authenticate', 'Flytedesk\Accounting\Http\AccountingExampleController@authenticate');
    Route::get('successUrl', 'Flytedesk\Accounting\Http\AccountingExampleController@successUrl');
    Route::get('oauthUrl', 'Flytedesk\Accounting\Http\AccountingExampleController@oauthUrl');
    Route::get('menuUrl', 'Flytedesk\Accounting\Http\AccountingExampleController@menuUrl');
    
    Route::get('tweakBook', 'Flytedesk\Accounting\Http\AccountingExampleController@test');


});

