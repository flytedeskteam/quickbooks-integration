<?php

namespace Accounting;

class Tweakbooks implements AccountingInterface
{
	public function __construct()
	{
		//
	}

	public function test()
	{
		return 'test for Tweakbooks';
	}

	public function invoiceCreate()
	{
		//
	}

	public function invoiceUpdate()
	{
		//
	}

	public function invoiceRead()
	{
		//
	}

	public function invoiceDelete()
	{
		//
	}

	public function invoiceLineAdd()
	{
		//
	}

	public function invoiceLineRemove()
	{
		//
	}

	public function invoiceSend()
	{

	}

	public function invoicePaymentAccept()
	{

	}

	public function vendorCreate()
	{
		//
	}
	
	public function vendorUpdate()
	{
		//
	}
	
	public function vendorRead()
	{
		//
	}
	
	public function vendorDelete()
	{
		//
	}
	
	public function POCreate()
	{
		//
	}
	
	public function POUpdate()
	{
		//
	}
	
	public function PORead()
	{
		//
	}
	
	public function PODelete()
	{
		//
	}
	
	public function POLineAdd()
	{
		//
	}
	
	public function POLineRemove()
	{
		//
	}
	
	public function customerCreate()
	{
		//
	}
	
	public function customerUpdate()
	{
		//
	}
	
	public function customerRead()
	{
		//
	}
	
	public function customerDelete()
	{
		//
	}
	
	public function customerPaymentAccept()
	{
		//
	}
	
}