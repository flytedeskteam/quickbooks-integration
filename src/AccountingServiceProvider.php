<?php

namespace Flytedesk\Accounting;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class AccountingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->loadViewsFrom(__DIR__ . '/views', 'accounting');
        // $this->app->router->group(['namespace' => 'Flytedesk\Accounting\Http'],
        //     function() {
                require __DIR__ . '/Http/routes.php';
            // });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * Bind AccountingInterface to Quickbooks. If you need to switch out something
         * other than Quickbooks, code it up to the AccountingInterface and make the
         * change right here
         */

        $this->mergeConfigFrom(
            __DIR__.'/config/quickbooks.php', 'quickbooks'
        );
        $this->app->bind('Flytedesk\Accounting\AccountingInterface', function () {
            return new \Flytedesk\Accounting\Quickbooks;
        });


         include_once __DIR__ . '/Http/routes.php';
         include_once __DIR__ . '/QuickBooks/customer_accounting.php';
         include_once __DIR__ . '/QuickBooks/vendor_accounting.php';
         include_once __DIR__ . '/QuickBooks/invoice_accounting.php';
         include_once __DIR__ . '/QuickBooks/bill_accounting.php';
         include_once __DIR__ . '/QuickBooks/billpayment_accounting.php';
         include_once __DIR__ . '/QuickBooks/utility_accounting.php';
         include_once __DIR__ . '/QuickBooks/employee_accounting.php';
         include_once __DIR__ . '/QuickBooks/payment_accounting.php';
         include_once __DIR__ . '/QuickBooks/purchase_order_accounting.php';
         include_once __DIR__ . '/QuickBooks/creditmemo_accounting.php';
         include_once __DIR__ . '/QuickBooks/vendorcredit_accounting.php';
        include_once __DIR__ . '/QuickBooks/item_accounting.php';

        // $this->app->make('Flytedesk\Accounting\Http\QuickbookController');

    }
}